module gitlab.com/aufawibowo/gohexagonal

go 1.15

require (
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gofiber/fiber/v2 v2.3.2
	github.com/golang/mock v1.4.4
	github.com/iDevoid/cptx v0.0.0-20210106134833-4c66b596980c
	github.com/iDevoid/stygis v0.0.0-20210106142535-a451819046a9
	github.com/lib/pq v1.0.0
	github.com/sirupsen/logrus v1.7.0
)
